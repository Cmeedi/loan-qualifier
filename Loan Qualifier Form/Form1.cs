﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loan_Qualifier_Form
{
    public partial class LoanQualifierForm : Form
    {
        public LoanQualifierForm()
        {
            InitializeComponent();
        }

        private void CheckQualificationsButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Names constatns
                const decimal MINIMUM_SALARY = 40000M;
                const int MINIMUM_YEARS_ON_JOB = 2;

                // Local variables
                decimal salary;
                int yearsOnJob;

                //get the salary and years on the job
                salary = decimal.Parse(AnnualSalarieTextBox.Text);
                yearsOnJob = int.Parse(YearsAtJobTextBox.Text);

                //Determine whether the user qualifies.
                if (salary >= MINIMUM_SALARY)
                {
                    if (yearsOnJob >= MINIMUM_YEARS_ON_JOB)
                    {
                        //The user qualifies
                        DecisionLabel.Text = "you qualifiy for the loan";
                    }
                    else
                    {
                        //The user does not qualify.
                        DecisionLabel.Text = "minimum years at current " + "job not met";
                    }
                }
                else
                {
                    // the user does not qualify
                    DecisionLabel.Text = "Minimum salary requirement " + "not met.";
                }
            }
            catch(Exception ex)
            {
                //Display an error message
                MessageBox.Show(ex.Message);
            }
        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            // Clear the TextBoxes and the decisionLabel
            AnnualSalarieTextBox.Text = "";
            YearsAtJobTextBox.Text = "";
            DecisionLabel.Text = "";

            //Reset focus
            AnnualSalarieTextBox.Focus();
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            //Close Form
            this.Close();
        }
    }
}
