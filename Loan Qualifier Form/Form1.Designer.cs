﻿namespace Loan_Qualifier_Form
{
    partial class LoanQualifierForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AnnualSalarieTextBox = new System.Windows.Forms.TextBox();
            this.YearsAtJobTextBox = new System.Windows.Forms.TextBox();
            this.AnnualSalarieLabel = new System.Windows.Forms.Label();
            this.YearsAtJobLabel = new System.Windows.Forms.Label();
            this.LoanDecisionLabel = new System.Windows.Forms.Label();
            this.DecisionLabel = new System.Windows.Forms.Label();
            this.CheckQualificationsButton = new System.Windows.Forms.Button();
            this.ClearButton = new System.Windows.Forms.Button();
            this.ExitButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // AnnualSalarieTextBox
            // 
            this.AnnualSalarieTextBox.Location = new System.Drawing.Point(196, 12);
            this.AnnualSalarieTextBox.Name = "AnnualSalarieTextBox";
            this.AnnualSalarieTextBox.Size = new System.Drawing.Size(100, 22);
            this.AnnualSalarieTextBox.TabIndex = 0;
            // 
            // YearsAtJobTextBox
            // 
            this.YearsAtJobTextBox.Location = new System.Drawing.Point(196, 40);
            this.YearsAtJobTextBox.Name = "YearsAtJobTextBox";
            this.YearsAtJobTextBox.Size = new System.Drawing.Size(100, 22);
            this.YearsAtJobTextBox.TabIndex = 1;
            // 
            // AnnualSalarieLabel
            // 
            this.AnnualSalarieLabel.AutoSize = true;
            this.AnnualSalarieLabel.Location = new System.Drawing.Point(90, 15);
            this.AnnualSalarieLabel.Name = "AnnualSalarieLabel";
            this.AnnualSalarieLabel.Size = new System.Drawing.Size(100, 17);
            this.AnnualSalarieLabel.TabIndex = 2;
            this.AnnualSalarieLabel.Text = "Annual Salarie";
            // 
            // YearsAtJobLabel
            // 
            this.YearsAtJobLabel.AutoSize = true;
            this.YearsAtJobLabel.Location = new System.Drawing.Point(57, 43);
            this.YearsAtJobLabel.Name = "YearsAtJobLabel";
            this.YearsAtJobLabel.Size = new System.Drawing.Size(133, 17);
            this.YearsAtJobLabel.TabIndex = 3;
            this.YearsAtJobLabel.Text = "Years at current job";
            // 
            // LoanDecisionLabel
            // 
            this.LoanDecisionLabel.AutoSize = true;
            this.LoanDecisionLabel.Location = new System.Drawing.Point(12, 77);
            this.LoanDecisionLabel.Name = "LoanDecisionLabel";
            this.LoanDecisionLabel.Size = new System.Drawing.Size(98, 17);
            this.LoanDecisionLabel.TabIndex = 4;
            this.LoanDecisionLabel.Text = "Loan Decision";
            // 
            // DecisionLabel
            // 
            this.DecisionLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DecisionLabel.Location = new System.Drawing.Point(12, 99);
            this.DecisionLabel.Name = "DecisionLabel";
            this.DecisionLabel.Size = new System.Drawing.Size(340, 42);
            this.DecisionLabel.TabIndex = 5;
            // 
            // CheckQualificationsButton
            // 
            this.CheckQualificationsButton.Location = new System.Drawing.Point(19, 155);
            this.CheckQualificationsButton.Name = "CheckQualificationsButton";
            this.CheckQualificationsButton.Size = new System.Drawing.Size(97, 47);
            this.CheckQualificationsButton.TabIndex = 6;
            this.CheckQualificationsButton.Text = "Check Qualifications";
            this.CheckQualificationsButton.UseVisualStyleBackColor = true;
            this.CheckQualificationsButton.Click += new System.EventHandler(this.CheckQualificationsButton_Click);
            // 
            // ClearButton
            // 
            this.ClearButton.Location = new System.Drawing.Point(122, 155);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(104, 47);
            this.ClearButton.TabIndex = 7;
            this.ClearButton.Text = "Clear";
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // ExitButton
            // 
            this.ExitButton.Location = new System.Drawing.Point(232, 155);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(111, 47);
            this.ExitButton.TabIndex = 8;
            this.ExitButton.Text = "Exit";
            this.ExitButton.UseVisualStyleBackColor = true;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // LoanQualifierForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(364, 223);
            this.Controls.Add(this.ExitButton);
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.CheckQualificationsButton);
            this.Controls.Add(this.DecisionLabel);
            this.Controls.Add(this.LoanDecisionLabel);
            this.Controls.Add(this.YearsAtJobLabel);
            this.Controls.Add(this.AnnualSalarieLabel);
            this.Controls.Add(this.YearsAtJobTextBox);
            this.Controls.Add(this.AnnualSalarieTextBox);
            this.Name = "LoanQualifierForm";
            this.Text = "Loan Qualifier";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox AnnualSalarieTextBox;
        private System.Windows.Forms.TextBox YearsAtJobTextBox;
        private System.Windows.Forms.Label AnnualSalarieLabel;
        private System.Windows.Forms.Label YearsAtJobLabel;
        private System.Windows.Forms.Label LoanDecisionLabel;
        private System.Windows.Forms.Label DecisionLabel;
        private System.Windows.Forms.Button CheckQualificationsButton;
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.Button ExitButton;
    }
}

